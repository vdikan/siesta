module m_psop
  use m_kbgen,    only: kbgen
  use m_localgen, only: compute_vlocal_chlocal
  use psop_params, only: nkbmx, lmaxd, nrmax
  public
end module m_psop
