#
# Inspired by the DFTB+ project
# Stops the code if the source and the build folders are identical.
#
function(siesta_util_ensure_out_of_source_build)

  get_filename_component(srcdir "${CMAKE_CURRENT_SOURCE_DIR}" REALPATH)
  get_filename_component(bindir "${CMAKE_CURRENT_BINARY_DIR}" REALPATH)

  if("${srcdir}" STREQUAL "${bindir}")
    message(FATAL_ERROR
      "It is not allowed to configure and build this project from its source folder. Please, create a \
separate build directory and invoke CMake from that directory.")
  endif()

endfunction()

#[==========================[
This function will push/pop variables to a global variable
called SIESTA_*_SUFFIX.
This will enable one to temporarily change executable suffixes
when subsequent usages are limited.
The * should be one(or multiple) of:
- EXECUTABLE
- SHARED_LIBRARY
- STATIC_LIBRARY
#]==========================]
message(VERBOSE "Using SIESTA_SUFFIX=${SIESTA_SUFFIX}")
message(VERBOSE "Using SIESTA_EXECUTABLE_SUFFIX=${SIESTA_EXECUTABLE_SUFFIX}")
message(VERBOSE "Using SIESTA_SHARED_LIBRARY_SUFFIX=${SIESTA_SHARED_LIBRARY_SUFFIX}")
message(VERBOSE "Using SIESTA_STATIC_LIBRARY_SUFFIX=${SIESTA_STATIC_LIBRARY_SUFFIX}")
set(SIESTA_EXECUTABLE_SUFFIXES "" CACHE INTERNAL "List of executable suffixes")
set(SIESTA_SHARED_LIBRARY_SUFFIXES "" CACHE INTERNAL "List of shared library suffixes")
set(SIESTA_STATIC_LIBRARY_SUFFIXES "" CACHE INTERNAL "List of static library suffixes")
function(siesta_suffix)
  set(options APPEND NEW POP PREPEND)
  set(oneValueArgs SUFFIX)
  set(multiValueArgs VARIABLES)
  cmake_parse_arguments(_esop "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  # Get default suffix
  if(NOT DEFINED _esop_SUFFIX)
    list(LENGTH _esop_UNPARSED_ARGUMENTS nargs)
    if(nargs GREATER 0)
      list(POP_FRONT _esop_UNPARSED_ARGUMENTS _esop_SUFFIX)
    endif()
  endif()
  if(NOT DEFINED _esop_VARIABLES)
    set(_esop_VARIABLES "EXECUTABLE" "SHARED_LIBRARY" "STATIC_LIBRARY")
  endif()
  set(allowed_var "EXECUTABLE" "SHARED_LIBRARY" "STATIC_LIBRARY")
  foreach(var IN LISTS _esop_VARIABLES)
    if(NOT ${var} IN_LIST allowed_var)
      message(FATAL_ERROR "${CMAKE_CURRENT_FUNCTION} expected VARIABLES to be one of ${allowed_var}, got ${var}")
    endif()
  endforeach()

  # Only one of
  #  - APPEND
  #  - NEW
  #  - POP
  #  - PREPEND
  # may be supplied
  set(defined FALSE)
  foreach(opt1 IN LISTS options)
    foreach(opt2 IN LISTS options)
      if("${opt1}" STREQUAL "${opt2}")
        if(_esop_${opt1})
          set(defined TRUE)
        endif()
      elseif(_esop_${opt1} AND _esop_${opt2})
        message(FATAL_ERROR "${CMAKE_CURRENT_FUNCTION}: both ${opt1} and ${opt2} argument found, only 1 of them allowed.")
      endif()
    endforeach()
  endforeach()
  if(NOT ${defined})
    message(FATAL_ERROR "${CMAKE_CURRENT_FUNCTION}: none of ${options} are defined, please at least supply one.")
  endif()
  
  foreach(var IN LISTS _esop_VARIABLES)

    set(cmake_var "CMAKE_${var}_SUFFIX")
    set(siesta_var "SIESTA_${var}_SUFFIXES")
    message(DEBUG "Current ${siesta_var}=${${siesta_var}}")
    message(DEBUG "Current ${cmake_var}=${${cmake_var}}")

    set(old "${${cmake_var}}")
    if(_esop_POP)
      list(POP_BACK ${siesta_var} new)
    else()
      # one of the changing of the variable
      if(_esop_APPEND)
        set(new "${old}${_esop_SUFFIX}")
      elseif(_esop_NEW)
        set(new "${_esop_SUFFIX}")
      elseif(_esop_PREPEND)
        set(new "${_esop_SUFFIX}${old}")
      endif()
      set(${siesta_var} "${${siesta_var}};${new}")
    endif()

    # Define the variables in parent scope
    set(${siesta_var} "${${siesta_var}}" PARENT_SCOPE)
    set(${cmake_var} "${new}" PARENT_SCOPE)

  endforeach()

endfunction()

macro(siesta_suffix_install)
  siesta_suffix(
    ${SIESTA_SUFFIX}
    PREPEND
    )
  siesta_suffix(
    ${SIESTA_EXECUTABLE_SUFFIX}
    PREPEND
    VARIABLES EXECUTABLE
    )
  siesta_suffix(
    ${SIESTA_SHARED_LIBRARY_SUFFIX}
    PREPEND
    VARIABLES SHARED_LIBRARY
    )
  siesta_suffix(
    ${SIESTA_STATIC_LIBRARY_SUFFIX}
    PREPEND
    VARIABLES STATIC_LIBRARY
    )
endmacro()
macro(siesta_suffix_uninstall)
  siesta_suffix(
    ${SIESTA_SUFFIX}
    POP
    )
  siesta_suffix(
    ${SIESTA_EXECUTABLE_SUFFIX}
    POP
    VARIABLES EXECUTABLE
    )
  siesta_suffix(
    ${SIESTA_SHARED_LIBRARY_SUFFIX}
    POP
    VARIABLES SHARED_LIBRARY
    )
  siesta_suffix(
    ${SIESTA_STATIC_LIBRARY_SUFFIX}
    POP
    VARIABLES STATIC_LIBRARY
    )
endmacro()

#[==========================[
Function to dynamically add a directory as a build-dependency
if the directory exists and has some files in it
The number of files should probably be tweaked, well.

This routine accepts a set of arguments to handle how sub-directories
are added.

  DIRECTORY : required
    which directory to dynamically add
  OPTION : required
    name of the option that is exposed to the user
  NITEMS : required
    number of items that should be in DIRECTORY before it is eligeble for
    adding as a sub-project.
  HELP : optional
    help text exposed for the OPTION, defaults to a description of the OPTION and DIRECTORY
  DEFAULT : optional
    a default value of OPTION in case it is eligeble for addition.
    Defaults to TRUE.

#]==========================]
function(siesta_add_subdirectory_option)
  set(options "")
  set(oneValueArgs DIRECTORY OPTION HELP NITEMS DEFAULT)
  set(multiValueArgs "")
  cmake_parse_arguments(_asop "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if(NOT DEFINED _asop_DIRECTORY)
    message(FATAL_ERROR "siesta_add_subdirectory_option requires DIRECTORY argument")
  endif()
  if(NOT DEFINED _asop_OPTION)
    message(FATAL_ERROR "siesta_add_subdirectory_option requires OPTION argument")
  endif()
  if(NOT DEFINED _asop_NITEMS)
    set(_asop_NFILES 1)
    message(VERBOSE "siesta_add_subdirectory_option set NITEMS to 1 (more than 1 file+directory in DIRECTORY)")
  endif()
  if(NOT DEFINED _asop_DEFAULT)
    set(_asop_DEFAULT TRUE)
    message(VERBOSE "siesta_add_subdirectory_option set DEFAULT to TRUE if the directory exists")
  endif()
  if(NOT DEFINED _asop_HELP)
    set(_asop_HELP
      "Include support for option ${_asop_OPTION} with a default value of ${_asop_DEFAULT} "
      "if the folder ${_asop_DIRECTORY} exists with >=${_asop_NITEMS} files+directories present.")
  endif()

  message(VERBOSE "Checking directory ${_asop_DIRECTORY} for content")

  file(GLOB _result
    LIST_DIRECTORIES TRUE
    "${_asop_DIRECTORY}/*"
    )
  list(LENGTH _result _result_len)
  message(VERBOSE "Directory ${_asop_DIRECTORY} contains ${_result_len} files and directories")
  if(_result_len GREATER _asop_NITEMS)
    option(${_asop_OPTION} "${_asop_HELP}" ${_asop_DEFAULT})
  else()
    set(${_asop_OPTION} FALSE CACHE BOOL "${_asop_HELP}" FORCE)
  endif()
  if ( ${${_asop_OPTION}} )
    message(STATUS "Adding support with ${_asop_OPTION}=TRUE")
    list(APPEND CMAKE_MESSAGE_INDENT "  ")
    add_subdirectory("${_asop_DIRECTORY}")
    list(POP_BACK CMAKE_MESSAGE_INDENT)
  endif()
endfunction()
