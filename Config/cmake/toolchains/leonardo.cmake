##- Toolchain file for use in Leonardo at CINECA, Italy
##-
##- Before you use this file, set the environment modules as follows in the shell.
##- (You can cut the following, put it in a toolchain.sh file, and do
##-      . toolchain.sh
##- )
##-
# module load  gcc/11.3.0
# module load  openblas/0.3.21--gcc--11.3.0
# module load  openmpi/4.1.4--gcc--11.3.0-cuda-11.8
# module load  netlib-scalapack/2.2.0--openmpi--4.1.4--gcc--11.3.0
# module load  netcdf-c/4.9.0--openmpi--4.1.4--gcc--11.3.0
# module load  netcdf-fortran/4.6.0--openmpi--4.1.4--gcc--11.3.0
# module load  libxc/6.2.2--gcc--11.3.0-cuda-11.8
# module load  fftw/3.3.10--openmpi--4.1.4--gcc--11.3.0
# module load  cuda/11.8
##-
#
# You might need to load modules for ELSI, ELPA, etc, as needed
#

# This seems to be needed because Openblas is mistakenly used in place of Scalapack...

set(SCALAPACK_LIBRARY "-L/leonardo/prod/spack/03/install/0.19/linux-rhel8-icelake/gcc-11.3.0/netlib-scalapack-2.2.0-vc35df24ev6666zfv6nfbqshykjbjbkh/lib -lscalapack" CACHE STRING "scalapack library")




