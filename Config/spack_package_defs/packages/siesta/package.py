from spack import *


class Siesta(CMakePackage):
    """An efficient DFT simulation code (recipe from Siesta-Project)"""

    homepage = "https://siesta-project.org/siesta"

    git = 'https://gitlab.com/siesta-project/siesta.git'

    # You can edit this and the above repo to experiment
    # with different versions
    version('master', branch='master')
    version('5.0.0-beta1', commit='baa3f85607a12cf')
    version('5.0.0-beta1.5', commit='ee499a7915ee678')
    version('5.0.0-beta2', tag='5.0.0-beta2')
    version('5.0.0', tag='v5.0.0')
    
    # Only a limited selection of variants for now,
    # pending spack recipes for other dependencies
    
    variant('mpi', default=False, description='Use MPI')
    variant('openmp', default=False, description='Build with OpenMP support (very few utilities will benefit)')
    variant('netcdf', default=False, description='Use NetCDF')
    variant('libxc', default=False, description='Use libxc')
    variant('elpa', default=False, description='Use ELPA library (native interface)')
    variant('fftw', default=True, description='Use FFTW library (needed only for STM/ol-stm)')
    variant('dftd3', default=True,  description='Support for DFT-D3 corrections')
    variant('flook', default=True,  description='Use flook')
    variant('gridsp', default=False,  description='Use single-precision grid operations')
    variant('pexsi', default=False,  description='Use (native) PEXSI interface')
    variant('wannier90', default=False,  description='Use Wannier90 on-the-fly wrapper')

    depends_on('cmake@3.17.0:', type='build')

    # generator = 'Ninja'
    # depends_on('ninja', type='build')
    
    depends_on('lapack')
    depends_on('xmlf90@1.6.3:')
    depends_on('libpsml@2.0.1:')
    depends_on('libfdf@0.5.1:')
    depends_on('mpi', when='+mpi')
    depends_on('scalapack', when='+mpi')
    depends_on('netcdf-fortran', when='+netcdf')
    depends_on('libxc@4:5', when='+libxc')
    depends_on('libgridxc@2.0.1:')
    depends_on('elpa@2020.05.001~openmp', when='+elpa')
    depends_on('fftw@3.3.0:', when='+fftw')
    depends_on('pexsi@2.0.0:', when='+pexsi')

    # Conflicts between dependency variants and SIESTA variants

    # Grid single-point variant has to match for libgridxc and siesta
    conflicts('libgridxc~gridsp', when='+gridsp')
    conflicts('libgridxc+gridsp', when='-gridsp')

    # If SIESTA uses libxc, then libgridxc has to have libxc support (not vice versa)
    conflicts('libgridxc~libxc', when='+libxc')

    # If SIESTA uses MPI, then libgridxc has to be compiled with MPI
    conflicts('libgridxc~mpi', when='+mpi')

    def cmake_args(self):
       args = [
            self.define_from_variant('WITH_MPI', 'mpi'),
            self.define_from_variant('WITH_OPENMP', 'openmp'),
            self.define_from_variant('WITH_LIBXC', 'libxc'),
            self.define_from_variant('WITH_NETCDF', 'netcdf'),
            self.define_from_variant('WITH_ELPA', 'elpa'),
            self.define_from_variant('WITH_PEXSI', 'pexsi'),
            self.define_from_variant('WITH_FFTW', 'fftw'),
            self.define_from_variant('WITH_DFTD3', 'dftd3'),
            self.define_from_variant('WITH_FLOOK', 'flook'),
            self.define_from_variant('WITH_GRID_SP', 'gridsp'),
            self.define_from_variant('WITH_WANNIER90', 'wannier90'),
       ]

       return args

