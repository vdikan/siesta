#

siesta_subtest(default_basis IS_SIMPLE LABELS simple)
siesta_subtest(fireballs)
siesta_subtest(nodes)
siesta_subtest(bessel)
siesta_subtest(bessel_rich)
#siesta_subtest(filteret)
siesta_subtest(custom_softbasis)
siesta_subtest(charge_confinement)
siesta_subtest(ghost_atom)